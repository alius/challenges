from __future__ import print_function
import sys
from random import randint

"""
Author: Artjom Vassiljev
email: artjom@protonmail.com
"""

class Board(object):
    def __init__(self, size):
        self.board = []
        self.size = size
        for _ in range(size[0]-1):
            row = [True]
            row += [False for _ in range(1, size[0]-1)]
            row.append(True)
            self.board.append(row)
        self.board.append([True for _ in range(size[0])])

    def add_block(self, block):
        self.block = block

    def rotate_block(self, cw=True):
        """ Rotates the block either clockwise or counter clockwise
        """
        self.block.rotate(cw)

    def move_block(self, left=True):
        """ Moves the block left or right
        """
        self.block.move_side(left)
        for x, row in enumerate(self.board):
            for y, col in enumerate(row):
                if col and self.block.intersect(x, y):
                    self.block.move_side(not left)
                    return False
        return True

    def process(self):
        """ Moves the block down and then check if it hits the bottom or
        any other block.
        Updates the board so that it can be redrawn
        """
        self.block.move_down()
        for x, row in enumerate(self.board):
            for y, col in enumerate(row):
                if col and self.block.intersect(x, y) \
                        and not self.block.stopped:
                    self.block.move_up()
                    self.block.stopped = True
                    break
        if self.block.stopped:
            # Block has hit the bottom or landed on another block
            for x, y in self.block.position:
                self.board[x][y] = True

        new_board = []
        for i, row in enumerate(self.board):
            if all(row) and i != self.size[1]-1:
                new_board.insert(0, [False for _ in range(self.size[1])])
            else:
                new_board.append(row)
        self.board = new_board

    def draw(self):
        """ Redraw the board
        """
        block_pos = self.block.position
        for x, row in enumerate(self.board):
            for y, col in enumerate(row):
                if col or (x,y) in block_pos:
                    print("*", end="")
                else:
                    print(" ", end="")
            print("")

    def is_game_over(self):
        """ Checks the height of the tower
        """
        height = 0
        for row in self.board:
            if any(row[1:19]):
                height +=1
        if height+1 == len(self.size):
            return True
        return False


class Block(object):
    stopped = False
    x = 0
    y = 0

    def __init__(self, board_size):
        self.board_size = board_size
        self.y = randint(1, board_size[1]-len(self.location))
        self.x = -1

    def rotate(self, cw=True):
        """ Rotates the block
        """
        l1, l2 = len(self.location[0]), len(self.location)
        rotated = [[False for _ in range(l1)] for _ in range(l2)]
        for x in range(l2):
            for y in range(l1):
                if cw:
                    rotated[(len(rotated)-1)-y][x] = self.location[x][y]
                else:
                    rotated[y][len(rotated)-1-x] = self.location[x][y]
        self.location = rotated

    @property
    def position(self):
        """ Returns the position of the block on the board
        """
        pos = []
        for row in range(len(self.location)):
            for col in range(len(self.location[1])):
                if self.location[row][col]:
                    pos.append((self.x+row, self.y+col))
        return pos

    def intersect(self, x, y):
        """ Checks if the (x,y) pair intersects with the block
        """
        return (x,y) in self.position

    def move_down(self):
        """ Moves block 1 row down
        """
        self.x += 1

    def move_up(self):
        """ Moves block 1 row up. Needed for undoing during Board.process()
        """
        self.x -= 1

    def move_side(self, move_left=True):
        """ Moves block either left or right
        """
        if move_left:
            self.y -= 1
        else:
            self.y += 1


class Stick(Block):
    """
    ****
    """
    location = [[True, True, True, True],
                [False, False, False, False],
                [False, False, False, False],
                [False, False, False, False]]


class Square(Block):
    """
    **
    **
    """
    location = [[True, True], [True, True]]

    def rotate(self, ccw=True):
        return True


class LeftSnake(Block):
    """
     *
    **
    *
    """
    location = [[False, True, False], [True, True, False], [True, False, False]]


class Gamma(Block):
    """
     *
     *
    **
    """
    location = [[False, True, False], [False, True, False], [True, True, False]]


class Alpha(Block):
    """
    *
    *
    **
    """
    location = [[True, False, False], [True, False, False], [True, True, False]]


BLOCKS = [Stick, Square, LeftSnake, Gamma, Alpha]
BOARD_SIZE = (20, 20)
block = BLOCKS[randint(0, len(BLOCKS)-1)](BOARD_SIZE)
board = Board(BOARD_SIZE)
board.add_block(block)
board.process()
board.draw()

while True:
    proceed = False
    cmd = raw_input("Your command: ").strip()
    if cmd == "q":
        break
    elif cmd == "a":
        # move left
        proceed = board.move_block(left=True)
    elif cmd == "d":
        # move right
        proceed = board.move_block(left=False)
    elif cmd == "w":
        # rotate ccw
        board.rotate_block(cw=False)
        proceed = True
    elif cmd == "s":
        # rotate cw
        board.rotate_block(cw=True)
        proceed = True
    elif cmd == "c":
        proceed = True

    if not proceed:
        print("Invalid input, try again")
    else:
        board.process()
        print("")
        board.draw()
        if block.stopped:
            if board.is_game_over():
                print("Gave Over")
                sys.exit(0)
            # block has reached the bottom, create new one
            block = BLOCKS[randint(0, len(BLOCKS)-1)](BOARD_SIZE)
            board.add_block(block)
