#include <stdio.h>
#include <stdint.h>

/**
 * author: Artiom Vasiliev
 *
 * The size of the Date field is 8 bytes: "DDMMYYYY". The goal is to minimize
 * the size of the Date field.
 */

/**
 * The first and simplest attempt: a structure with 3 integers:
 *  - unsigned byte for day
 *  - unsigned byte for month
 *  - signed short for the year
 *  Size: 4 bytes, so we reduce the size by 50%
 */
typedef struct {
    uint8_t day;
    uint8_t month;
    int16_t year;
} FatDate;

/**
 * Second attempt: a structure with bit fields:
 *  - 5 bits for the day to store maximum 31 day
 *  - 4 bits for the month to store maximum 12 months
 *  - 15 bits for the year
 *  Size: 4 bytes, since day and month don't fit together into a single byte
 */
typedef struct {
    uint8_t day:5;
    uint8_t month:4;
    uint16_t year:15;
} BitDate;

/**
 * Third attempt: this requires a separate class to work with its own methods
 * We need 24 bits to store day (5), month (4), and year (15), so can use
 * 3 bytes, and use functions to extract day, month, and year.
 *  - part1: lower 5 bits used for the day, remaining 3 for the month
 *  - part2: lower 1 bit used for the month, remaining 7 for the year
 *  - part3: all bits used for the year
 */
typedef struct {
    uint8_t part1;
    uint8_t part2;
    uint8_t part3;
} UnifiedBitDate;

// sets the first 5 bits to store the day, no overflow checks
void setUnifiedDay(UnifiedBitDate *s, uint8_t day) {
    for (char i = 0; i < 5; i++) {
        // (day >> i & 1) - reads the i'th bit
        // the rest of the expression sets the i'th bit in s->part1
        s->part1 ^= (-(day >> i & 1) ^ s->part1) & (1 << i);
    }
}

// reads the day from the struct
uint8_t getUnifiedDay(UnifiedBitDate *s) {
    uint8_t day;
    for (char i = 0; i < 5; i++) {
        day ^= (-(s->part1 >> i & 1) ^ day) & (1 << i);
    }
    return day;
}

// sets the month
void setUnifiedMonth(UnifiedBitDate *s, uint8_t month) {
    for (char i = 0; i < 3; i++) {
        s->part1 ^= (-(month >> i & 1)) & (1 << (i+5));
    }

    s->part2 ^= -(month >> 3) & 1;
}

// reads the month
uint8_t getUnifiedMonth(UnifiedBitDate *s) {
    uint8_t month;
    for (char i = 0; i < 3; i++) {
        month ^= (-((s->part1 >> (i+5)) & 1) ^ month) & (1 << i);
    }

    month ^= (-(s->part2 & 1) ^ month) & 1;

    return month;
}

// sets the year
void setUnifiedYear(UnifiedBitDate *s, uint16_t year) {
    for (char i = 1; i < 8; i++) {
        s->part2 ^= (-(year >> (i-1) & 1)) & (1 << i);
    }

    for (char i = 0; i < 8; i++) {
        s->part3 ^= (-(year >> (i+8) & 1)) & (1 << i);
    }
}

// reads the year
uint16_t getUnifiedYear(UnifiedBitDate *s) {
    uint16_t year;
    for (char i = 1; i < 8; i++) {
        year ^= (-((s->part2 >> i) & 1) ^ year) & (1 << (i-1));
    }

    for (char i = 0; i < 8; i++) {
        year ^= (-((s->part3 >> i) & 1) ^ year) & (1 << (i+8));
    }

    return year;
}


int main(void) {
    printf("An attempt at minimizing the size of the Date field\n");
    printf("FatDate size: %lu\n", sizeof(FatDate));
    printf("BitDate size: %lu\n", sizeof(BitDate));
    printf("UnifiedBitDate size: %lu\n", sizeof(UnifiedBitDate));

    return 0;
}
